from PySide.QtGui import *
from PySide.QtCore import *
from AdditionalFunctions import *
from Construction import *


class drawWidget(QWidget):
    def __init__(self):
        super(drawWidget, self).__init__()
        self.begin = 0
        self.end = 1000
        self.selectedL = 0
        self.kL = 1
        self.painter = QPainter()
        self.resize(400, 400)

        self.ButLeft = 0



    def paintEvent(self, event):
        # rec = event.rect()
        self.painter.begin(self)

        self.SelectedCornel()
        self.drawFrame()
        self.ScaleL()
        self.drawCornels()
        self.draw_seal()

        self.painter.end()

    def drawFrame(self):
        self.painter.setPen(QPen(QBrush(Qt.black), 1))
        self.painter.drawRect(0, 0, 800, 410)
        self.painter.drawRect(3, 3, 794, 404)


    def ScaleL(self):
        k = 0
        for i in range(self.begin, int(self.end * (1/self.kL))):
            k += 1
            if i > 1 or i < -1:
                self.painter.drawText(100 * k * self.kL, 400, str(i) + ' L')
            if i == 1:
                self.painter.drawText(100 * k * self.kL, 400, ' L')
            if i == -1:
                self.painter.drawText(100 * k * self.kL, 400, ' - L')
            if i == 0:
                self.painter.drawText(100 * k * self.kL, 400, '0')

    def LeftScene(self):
        self.begin -= 1
        self.end -= 1
        self.update()

    def RightScene(self):
        self.begin += 1
        self.end += 1

        self.update()

    def drawCornels(self):
        for i in range(len(Model.cornels)):
            # Рисование стержней
            # self.SelectedCornel()
            self.drawCornel(i)
            self.drawFjLeft(i)
            self.drawFjRight(i)
            self.draw_qi(i)

            # print(100 + 25*Model.cornels[i].A/2, ' - ', 50 + 25*Model.cornels[i].A)


    def drawCornel(self, index):
        self.painter.drawRect((0 - self.begin * 100 + 100 + Sum(index) * 100)*self.kL, 100 - 25 * Model.cornels[index].A, (100 * Model.cornels[index].L)*self.kL, 50 * Model.cornels[index].A)

    def drawFjLeft(self, index):
        if Model.cornels[index].FjLeft > 0:
            self.painter.setPen(QPen(QBrush(QColor(123, 0, 28)), 4))
            self.painter.drawLine((0 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 100, (35 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 100)

            ArrowLeft = QPolygonF()
            ArrowLeft.append(QPointF((22 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 104))
            ArrowLeft.append(QPointF((35 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 100))
            ArrowLeft.append(QPointF((22 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 95))

            self.painter.drawPolygon(ArrowLeft)

            path = QPainterPath()
            path.addPolygon(ArrowLeft)

            brush = QBrush()
            brush.setColor(Qt.red)
            brush.setStyle(Qt.SolidPattern)

            self.painter.fillPath(path, brush)
            self.painter.setPen(QPen(QBrush(Qt.black), 1))

        if Model.cornels[index].FjLeft < 0:
            self.painter.setPen(QPen(QBrush(QColor(123, 0, 28)), 4))
            self.painter.drawLine((0 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 100, (-35 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 100)

            ArrowRight = QPolygonF()
            ArrowRight.append(QPointF((-22 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL,104))
            ArrowRight.append(QPointF((-35 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL,100))
            ArrowRight.append(QPointF((-22 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 95))

            self.painter.drawPolygon(ArrowRight)

            path = QPainterPath()
            path.addPolygon(ArrowRight)

            brush = QBrush()
            brush.setColor(Qt.red)
            brush.setStyle(Qt.SolidPattern)

            self.painter.fillPath(path, brush)
            self.painter.setPen(QPen(QBrush(Qt.black), 1))

    def drawFjRight(self, index):
        if Model.cornels[index].FjRight < 0:
            self.painter.setPen(QPen(QBrush(QColor(123, 0, 28)), 4))
            self.painter.drawLine((0 - self.begin * 100 + 100 + Sum(index) * 100 + 100 * Model.cornels[index].L) * self.kL, 100, (-35 - self.begin * 100 + 100 + Sum(index) * 100 + 100 * Model.cornels[index].L) * self.kL, 100)

            ArrowRight = QPolygonF()
            ArrowRight.append(QPointF((-22 - self.begin * 100 + 100 + Sum(index) * 100 + 100 * Model.cornels[index].L) * self.kL, 104))
            ArrowRight.append(QPointF((-35 - self.begin * 100 + 100 + Sum(index) * 100 + 100 * Model.cornels[index].L) * self.kL, 100))
            ArrowRight.append(QPointF((-22 - self.begin * 100 + 100 + Sum(index) * 100 + 100 * Model.cornels[index].L) * self.kL, 95))

            self.painter.drawPolygon(ArrowRight)

            path = QPainterPath()
            path.addPolygon(ArrowRight)

            brush = QBrush()
            brush.setColor(Qt.red)
            brush.setStyle(Qt.SolidPattern)

            self.painter.fillPath(path, brush)
            self.painter.setPen(QPen(QBrush(Qt.black), 1))

        if Model.cornels[index].FjRight > 0:
            self.painter.setPen(QPen(QBrush(QColor(123, 0, 28)), 4))
            self.painter.drawLine((0 - self.begin * 100 + 100 + Sum(index) * 100 + Model.cornels[index].L*100 + 1) * self.kL, 100,
                                  (35 - self.begin * 100 + 100 + Sum(index) * 100 + Model.cornels[index].L*100) * self.kL, 100)

            ArrowLeft = QPolygonF()
            ArrowLeft.append(QPointF((22 - self.begin * 100 + 100 + Sum(index) * 100 + Model.cornels[index].L*100) * self.kL, 104))
            ArrowLeft.append(QPointF((35 - self.begin * 100 + 100 + Sum(index) * 100 + Model.cornels[index].L*100) * self.kL, 100))
            ArrowLeft.append(QPointF((22 - self.begin * 100 + 100 + Sum(index) * 100 + Model.cornels[index].L*100) * self.kL, 95))

            self.painter.drawPolygon(ArrowLeft)

            path = QPainterPath()
            path.addPolygon(ArrowLeft)

            brush = QBrush()
            brush.setColor(Qt.red)
            brush.setStyle(Qt.SolidPattern)

            self.painter.fillPath(path, brush)
            self.painter.setPen(QPen(QBrush(Qt.black), 1))

    def draw_qi(self, index):
        if Model.cornels[index].qi > 0:
            self.painter.drawLine((0 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 100, (0 - self.begin * 100 + 100 + Sum(index) * 100 + 100 * Model.cornels[index].L) * self.kL, 100)

            for k in range(int(Model.cornels[index].L * 5 * self.kL)):
                ArrowLeft = QPolygonF()
                ArrowLeft.append(QPointF(7 + (- self.begin * 100 + 100 + Sum(index) * 100) * self.kL + k * 20, 103))
                ArrowLeft.append(QPointF(20 + (- self.begin * 100 + 100 + Sum(index) * 100) * self.kL + k * 20, 100))
                ArrowLeft.append(QPointF(7 + (- self.begin * 100 + 100 + Sum(index) * 100) * self.kL + k * 20, 96))

                self.painter.drawPolygon(ArrowLeft)

                path = QPainterPath()
                path.addPolygon(ArrowLeft)

                brush = QBrush()
                brush.setColor(Qt.black)
                brush.setStyle(Qt.SolidPattern)

                self.painter.fillPath(path, brush)

        if Model.cornels[index].qi < 0:
            self.painter.drawLine((0 - self.begin * 100 + 100 + Sum(index) * 100) * self.kL, 100, (0 - self.begin * 100 + 100 + Sum(index) * 100 + 100 * Model.cornels[index].L) * self.kL, 100)

            for k in range(int(Model.cornels[index].L * 5 * self.kL)):
                ArrowLeft = QPolygonF()
                ArrowLeft.append(QPointF(13 + (- self.begin * 100 + 100 + Sum(index) * 100) * self.kL + k * 20, 103))
                ArrowLeft.append(QPointF(0 + (- self.begin * 100 + 100 + Sum(index) * 100) * self.kL + k * 20, 100))
                ArrowLeft.append(QPointF(13 + (- self.begin * 100 + 100 + Sum(index) * 100) * self.kL + k * 20, 96))

                self.painter.drawPolygon(ArrowLeft)

                path = QPainterPath()
                path.addPolygon(ArrowLeft)

                brush = QBrush()
                brush.setColor(Qt.black)
                brush.setStyle(Qt.SolidPattern)

                self.painter.fillPath(path, brush)

        self.drawFjLeft(index)
        self.drawFjRight(index)


    def draw_seal(self):
        if Sum(len(Model.cornels)) != 0:
            if Model.Seal == 'Заделка слева':
                self.draw_sealLeft()
            if Model.Seal == 'Заделка справа':
                self.draw_sealRight()
            if Model.Seal == 'Заделка слева и справа':
                self.draw_sealLeft()
                self.draw_sealRight()


    def draw_sealLeft(self):
        seal = QPolygonF()
        seal.append(QPointF((-10 - self.begin * 100 + 100) * self.kL, 80))
        seal.append(QPointF((0 - self.begin * 100 + 100) * self.kL, 80))
        seal.append(QPointF((0 - self.begin * 100 + 100) * self.kL, 120))
        seal.append(QPointF((-10 - self.begin * 100 + 100) * self.kL, 120))

        self.painter.drawPolygon(seal)

        path = QPainterPath()
        path.addPolygon(seal)

        brush = QBrush()
        brush.setColor(Qt.black)
        brush.setStyle(Qt.SolidPattern)

        self.painter.fillPath(path, brush)

        for i in range(0, 5):
            self.painter.drawLine((-20 - self.begin * 100 + 100) * self.kL, 110 - i*8, (-10 - self.begin * 100 + 100) * self.kL, 120 - i*8)

    def draw_sealRight(self):
        seal = QPolygonF()
        seal.append(QPointF((0 - self.begin * 100 + 100 + Sum(len(Model.cornels))*100) * self.kL, 80))
        seal.append(QPointF((10 - self.begin * 100 + 100 + Sum(len(Model.cornels))*100) * self.kL, 80))
        seal.append(QPointF((10 - self.begin * 100 + 100 + Sum(len(Model.cornels))*100) * self.kL, 120))
        seal.append(QPointF((0 - self.begin * 100 + 100 + Sum(len(Model.cornels))*100) * self.kL, 120))

        self.painter.drawPolygon(seal)

        path = QPainterPath()
        path.addPolygon(seal)

        brush = QBrush()
        brush.setColor(Qt.black)
        brush.setStyle(Qt.SolidPattern)

        self.painter.fillPath(path, brush)

        for i in range(0, 5):
            self.painter.drawLine((10 - self.begin * 100 + 100 + Sum(len(Model.cornels))*100) * self.kL, 110 - i*7, (20 - self.begin * 100 + 100 + Sum(len(Model.cornels))*100) * self.kL, 120 - i*7)


    def SelectedCornel(self):
        if len(Model.cornels) > 0:
            if self.selectedL < len(Model.cornels):
                Rect = QRectF(((Sum(self.selectedL))*100 + 100 - self.begin * 100) * self.kL, 4, (100*Model.cornels[self.selectedL].L) * self.kL + 1, 403)

                path = QPainterPath()
                path.addRect(Rect)

                brush = QBrush()
                brush.setColor(QColor(224, 219, 215))
                brush.setStyle(Qt.SolidPattern)
                self.painter.fillPath(path, brush)

            self.update()

    def CreateConstruction(self):
        self.update()

    def SceneLess(self):
        if self.kL > 0.6:
            self.kL /= 2
            self.update()

    def SceneMore(self):
        if self.kL < 2:
            self.kL *= 2
            self.update()


    def drawN(self):
        for k in range(len(Model.cornels)):
            for x in range(Model.cornels[k].L*100+1):
                # for j in range(len(Model.cornels[i].L) * 10):
                Model.calculate_Nx(k, x/100)
                f = Model.cornels[k].N_x
                self.painter.drawPoint(x - self.begin * 100 + 100 + 100*Sum(k), 300 - f * 40 / (20/Model.cornels[k].N_L))

        self.painter.drawLine(100 - self.begin * 100, 300, (Sum(len(Model.cornels))+1) * 100, 300)


