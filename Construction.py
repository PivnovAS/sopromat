from numpy import *


class Cornel:
    def __init__(self):
        self.L = 0
        self.A = 0
        self.FjLeft = 0
        self.FjRight = 0
        self.qi = 0
        self.sMax = 0


        self.U_null = 0
        self.U_L = 0

        self.N_null = 0
        self.N_L = 0
        self.N_x = 0

        self.s_null = 0
        self.s_L = 0
        self.s_x = 0

        self.u_null = 0
        self.u_L = 0
        self.u_x = 0


class Construction:
    def __init__(self):
        self.cornels = []
        self.Seal = 'Заделка слева и справа'
        self.Matrix = []
        self.Answer = []
        self.Delta = []
        self.U = []

        self.N = []
        self.u = []

        self.errors = []

        self.Diagram = 0



    def addCornel(self):
        self.cornels.append(Cornel())


    def delCornel(self, index):
        self.cornels.pop(index)

    def clear(self):
        self.cornels.clear()

    def fill(self):
        # Запорлнение матрицы
        self.Matrix.clear()

        for i in range(len(self.cornels)+1):
            Row = []
            for j in range(len(self.cornels)+1):
                Row.append(0)
            self.Matrix.append(Row)


        for i in range(len(self.cornels) + 1):
            for j in range(len(self.cornels) + 1):
                if i == 0:
                    if self.Seal == 'Заделка слева' or self.Seal == 'Заделка слева и справа':
                        self.Matrix[0][0] = 1

                    if self.Seal == 'Заделка справа':
                        self.Matrix[0][0] = self.cornels[i].A / self.cornels[i].L
                        self.Matrix[0][1] = - self.cornels[i].A / self.cornels[i].L

                if (i > 0) and (i < len(self.cornels)):
                    if i == j:
                        self.Matrix[i][j-1] = - self.cornels[i-1].A / self.cornels[i-1].L
                        self.Matrix[i][j] = self.cornels[i].A / self.cornels[i].L + self.cornels[i-1].A / self.cornels[i-1].L
                        self.Matrix[i][j+1] = - self.cornels[i].A / self.cornels[i].L

                if i == len(self.cornels):
                    if self.Seal == 'Заделка справа' or self.Seal == 'Заделка слева и справа':
                        self.Matrix[len(self.cornels)][len(self.cornels)] = 1

                    if self.Seal == 'Заделка слева':
                        self.Matrix[len(self.cornels)][len(self.cornels)] = self.cornels[len(self.cornels) - 1].L / self.cornels[len(self.cornels) - 1].A
                        self.Matrix[len(self.cornels)][len(self.cornels) - 1] = - self.cornels[len(self.cornels) - 1].L / self.cornels[len(self.cornels) - 1].A

        # Заполнение ответов матрицы
        self.Answer.clear()

        for i in range(len(self.cornels) + 1):
            self.Answer.append(0)

        for i in range(len(self.cornels)+1):
            qi = 0
            if i == 0:
                qi = self.cornels[i].qi * self.cornels[i].L / 2
            if i != 0 and i != len(self.cornels):
                qi = self.cornels[i].qi * self.cornels[i].L / 2 + self.cornels[i - 1].qi * self.cornels[i - 1].L / 2
            if i == len(self.cornels):
                qi = self.cornels[i - 1].qi * self.cornels[i - 1].L / 2

            Fj = 0
            if i != 0 and i != len(self.cornels):
                Fj = self.cornels[i-1].FjRight
            if i == 0:
                Fj = self.cornels[i].FjLeft
            if i == len(self.cornels):
                Fj = self.cornels[i-1].FjRight

            self.Answer[i] = qi + Fj

        if self.Seal == 'Заделка слева':
            self.Answer[0] = 0
        if self.Seal == 'Заделка справа':
            self.Answer[-1] = 0
        if self.Seal == 'Заделка слева и справа':
            self.Answer[0] = 0
            self.Answer[-1] = 0



    def calculateDelta(self):
        self.Delta = linalg.solve(self.Matrix, self.Answer)

        for i in range(len(self.Delta)):
            self.Delta[i] = float(round(self.Delta[i], 2))


    def calculateU(self):
        self.U.clear()
        self.U.append(self.Delta[0])


        for i in range(1, len(self.Delta) - 1):
            self.U.append(self.Delta[i])
            self.U.append(self.Delta[i])

        self.U.append(self.Delta[-1])

        for i in range(len(self.cornels)):
            self.cornels[i].U_null = self.U[i * 2]
            self.cornels[i].U_L = self.U[i * 2 + 1]


    def calculateN(self):
        self.N.clear()
        for i in range(len(self.cornels)):
            self.N.append(self.cornels[i].A / self.cornels[i].L * (self.cornels[i].U_L - self.cornels[i].U_null) + self.cornels[i].qi * self.cornels[i].L / 2)
            self.N.append(self.cornels[i].A / self.cornels[i].L * (self.cornels[i].U_L - self.cornels[i].U_null) + self.cornels[i].qi * self.cornels[i].L / 2 * (-1))

        for i in range(len(self.cornels)):
            self.cornels[i].N_null = self.N[i * 2]
            self.cornels[i].N_L = self.N[i * 2 + 1]


    def qi(self, index, x):
        if x == 0:
            if index == 0:
                return self.cornels[index].qi
            if index != 0:
                return self.cornels[index].qi + self.cornels[index - 1].qi
        if x == 1:
            if index != len(self.cornels) - 1:
                return self.cornels[index].qi + self.cornels[index + 1].qi
            if index == len(self.cornels) - 1:
                return self.cornels[index].qi

    def calculate_s(self):
        for i in self.cornels:
            i.s_null = i.N_null / i.A
            i.s_L = i.N_L / i.A


    def calculate_u(self):
        for i in self.cornels:
            i.u_null = i.U_null
            i.u_L = i.U_L


    def calculate_ux(self, i, x):
        qi = self.cornels[i].qi

        self.cornels[i].u_x = self.cornels[i].u_null + x / self.cornels[i].L * (self.cornels[i].u_L - self.cornels[i].u_null) + qi * self.cornels[i].L * x / (2 * self.cornels[i].A) * (1 - x / self.cornels[i].L)

    def calculate_Nx(self, i, x):
        qi = self.cornels[i].qi

        self.cornels[i].N_x = self.cornels[i].A / self.cornels[i].L * (self.cornels[i].u_L - self.cornels[i].u_null) + qi * self.cornels[i].L / 2 * (1 - 2 * x / self.cornels[i].L)

    def calculate_sx(self, i, x):
        qi = self.cornels[i].qi
        Nx = self.cornels[i].A / self.cornels[i].L * (self.cornels[i].u_L - self.cornels[i].u_null) + qi * self.cornels[i].L / 2 * (1 - 2 * x / self.cornels[i].L)

        self.cornels[i].s_x = Nx / self.cornels[i].A


    def show_u(self):
        for i in range(int(len(self.N) / 2)):
            print('u(', i, ', 0) =', self.cornels[i].u_null, 'qL/A', ' |  u(', i, ', L) =', self.cornels[i].u_L, 'qL/A')


    def show_s(self):
        for i in range(int(len(self.N) / 2)):
            print('σ(', i, ', 0) =', self.cornels[i].s_null, 'qL/A', ' |  σ(', i, ', L) =', self.cornels[i].s_L, 'qL/A')


    def show_U(self):
        for i in range(int(len(self.U) / 2)):
            print('U(', i, ', 0) =', self.cornels[i].U_null, 'qL^2/AE', ' |  U(', i, ', L) =', self.cornels[i].U_L, 'qL^2/AE')


    def show_N(self):
        for i in range(int(len(self.N) / 2)):
            print('N(', i, ', 0) =', self.cornels[i].N_null, 'qL', ' |  N(', i, ', L) =', self.cornels[i].N_L, 'qL')
        # print(self.N)


    def showMatrix(self):
        print('Matrix: ')
        for i in range(len(self.Matrix)):
            print(self.Matrix[i], self.Answer[i])


    def show(self):
        for i in range(len(self.cornels)):
            print('Стержень ', i+1)
            print('L = ', self.cornels[i].L)
            print('A = ', self.cornels[i].A)
            print('FjLeft = ', self.cornels[i].FjLeft)
            print('FjRight = ', self.cornels[i].FjRight)
            print('qi = ', self.cornels[i].qi)


Model = Construction()

