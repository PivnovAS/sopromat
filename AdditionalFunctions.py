from Construction import *


# 1 - int,  2 - float,  3 - string
def is_digit(string):
    if string.isdigit():
        return 1
    else:
        try:
            float(string)
            return 2
        except ValueError:
            return 3


def Sum(index):
    s = 0
    for i in range(index):
        s += Model.cornels[i].L
    return s

