from Draw import *
from PySide.QtGui import *
from PySide.QtCore import *
from AdditionalFunctions import *
import Construction
import os
import pickle


class Main(QWidget):
    def __init__(self):
        super(Main, self).__init__()
        Grid = QGridLayout(self)
        self.CurrentCornel = 0

        self.cornels = QListWidget()
        Grid.addWidget(self.cornels, 0, 0, 1, 3)

        self.ButAdd = QPushButton('Add')
        self.ButDel = QPushButton('Delete')
        self.ButClear = QPushButton('Clear')
        Grid.addWidget(self.ButAdd, 1, 0)
        Grid.addWidget(self.ButDel, 1, 1)
        Grid.addWidget(self.ButClear, 1, 2)


        self.KinematicConditions = QComboBox()
        self.KinematicConditions.addItem('Заделка слева')
        self.KinematicConditions.addItem('Заделка справа')
        self.KinematicConditions.addItem('Заделка слева и справа')
        Grid.addWidget(self.KinematicConditions, 2, 0, 1, 3)


        self.labelL = QLabel('L')
        self.labelA = QLabel('A')
        self.labelFjLeft = QLabel('Fj Left')
        self.labelFjRight = QLabel('Fj Right')
        self.labelqi = QLabel('qi')
        self.label_sMax = QLabel('σ max')

        self.lineL = QLineEdit('0')
        self.lineA = QLineEdit('0')
        self.lineFjLeft = QLineEdit('0')
        self.lineFjRight = QLineEdit('0')
        self.lineqi = QLineEdit('0')
        self.line_sMax = QLineEdit('0')

        Grid.addWidget(self.labelL, 4, 0)
        Grid.addWidget(self.labelA, 5, 0)
        Grid.addWidget(self.labelFjLeft, 6, 0)
        Grid.addWidget(self.labelFjRight, 7, 0)
        Grid.addWidget(self.labelqi, 8, 0)
        Grid.addWidget(self.label_sMax, 9, 0)

        Grid.addWidget(self.lineL, 4, 1, 1, 2)
        Grid.addWidget(self.lineA, 5, 1, 1, 2)
        Grid.addWidget(self.lineFjLeft, 6, 1, 1, 2)
        Grid.addWidget(self.lineFjRight, 7, 1, 1, 2)
        Grid.addWidget(self.lineqi, 8, 1, 1, 2)
        Grid.addWidget(self.line_sMax, 9, 1, 1, 2)


        self.Save = QPushButton('Save')
        Grid.addWidget(self.Save, 10, 0, 1, 3)


        self.Draw = drawWidget()
        Grid.addWidget(self.Draw, 0, 4, 10, 4)
        self.Draw.setMinimumSize(801, 400)


        self.ButMoveSceneLeft = QPushButton('<<')
        Grid.addWidget(self.ButMoveSceneLeft, 10, 4)

        self.ButSceneLess = QPushButton('-')
        Grid.addWidget(self.ButSceneLess, 11, 4)


        self.ButCreateConstruction = QPushButton('Calculate system')
        Grid.addWidget(self.ButCreateConstruction, 10, 5, 1, 2)


        self.ButSceneMore = QPushButton('+')
        Grid.addWidget(self.ButSceneMore, 11, 7)


        self.ButMoveSceneRight = QPushButton('>>')
        Grid.addWidget(self.ButMoveSceneRight, 10, 7)

        self.Table = ResultTable()
        self.ButOpenTableResult = QPushButton('Open results')
        Grid.addWidget(self.ButOpenTableResult, 0, 8, 2, 3)
        self.connect(self.ButOpenTableResult, SIGNAL('clicked()'), self.openTableResult)

        self.textKindLoad = 'Продольные силы Nx'
        self.KindLoad = QComboBox()
        self.KindLoad.addItem('Продольные силы Nx')
        self.KindLoad.addItem('Нормальные напряжения σx')
        self.KindLoad.addItem('Перемещения ux')
        Grid.addWidget(self.KindLoad, 2, 8, 1, 3)

        self.ValueRod = QLabel('Value of rod: ')
        Grid.addWidget(self.ValueRod, 1, 8, 1, 3)

        self.lineValueRod = QLineEdit('0')
        Grid.addWidget(self.lineValueRod, 1, 9, 1, 2)

        self.butCalculate = QPushButton('Calculate')
        Grid.addWidget(self.butCalculate, 3, 8, 1, 3)

        self.ValueComponent = QLabel('')
        Grid.addWidget(self.ValueComponent, 5, 8, 1, 2)


        self.connect(self.ButAdd, SIGNAL('clicked()'), self.Add)
        self.connect(self.ButDel, SIGNAL('clicked()'), self.Del)
        self.connect(self.ButClear, SIGNAL('clicked()'), self.Clear)
        self.connect(self.Save, SIGNAL('clicked()'), self.setValue)

        self.cornels.itemClicked.connect(self.getValue)
        self.cornels.itemClicked.connect(self.currentCornel)
        self.KinematicConditions.activated.connect(self.setKC)
        self.KindLoad.activated.connect(self.setTextKindLoad)

        self.connect(self.ButMoveSceneLeft, SIGNAL('clicked()'), self.moveSceneLeft)
        self.connect(self.ButMoveSceneRight, SIGNAL('clicked()'), self.moveSceneRight)
        self.connect(self.ButCreateConstruction, SIGNAL('clicked()'), self.createConstruction)
        self.connect(self.ButSceneLess, SIGNAL('clicked()'), self.sceneLess)
        self.connect(self.ButSceneMore, SIGNAL('clicked()'), self.sceneMore)

        self.ButOpenProject = QPushButton('Open project')
        self.ButSaveProject = QPushButton('Save project')

        Grid.addWidget(self.ButOpenProject, 11, 0)
        Grid.addWidget(self.ButSaveProject, 11, 1)

        self.connect(self.ButOpenProject, SIGNAL('clicked()'), self.openProject)
        self.connect(self.ButSaveProject, SIGNAL('clicked()'), self.saveProject)
        self.connect(self.butCalculate, SIGNAL('clicked()'), self.resultComponent)

        self.setWindowTitle('САПР')
        self.setMinimumSize(1280, 500)


    def currentCornel(self):
        self.CurrentCornel = self.cornels.currentRow()
        self.Draw.selectedL = self.CurrentCornel

        # print(self.cornels.currentRow())


    def setKC(self):
        Construction.Model.Seal = self.KinematicConditions.currentText()


    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Delete:
            self.Del()

        if e.key() == Qt.Key_Right:
            self.Draw.RightScene()

        if e.key() == Qt.Key_Left:
            self.Draw.LeftScene()


    def Add(self):
        self.cornels.addItem('Стержень')
        Construction.Model.addCornel()

        Construction.Model.cornels[-1].L = 1
        Construction.Model.cornels[-1].A = 1
        Construction.Model.cornels[-1].FjLeft = 0
        if len(Construction.Model.cornels) > 1:
            Construction.Model.cornels[-1].FjLeft = Construction.Model.cornels[-2].FjRight
        Construction.Model.cornels[-1].FjRight = 0
        Construction.Model.cornels[-1].qi = 0
        Construction.Model.cornels[-1].sMax = 0


        self.Draw.update()

    def Del(self):
        if len(Construction.Model.cornels) != 0:
            if self.cornels.currentRow() == -1:
                self.cornels.takeItem(0)
                Construction.Model.delCornel(0)
            else:
                self.cornels.takeItem(self.CurrentCornel)
                Construction.Model.delCornel(self.CurrentCornel)
                if self.CurrentCornel > 0:
                    self.CurrentCornel -= 1
                else:
                    self.CurrentCornel = 0

        self.Draw.selectedL = self.CurrentCornel
        self.Draw.update()


    def Clear(self):
        if len(Construction.Model.cornels) != 0:
            self.cornels.clear()
            Construction.Model.clear()

            self.lineL.setText('0')
            self.lineA.setText('0')
            self.lineFjLeft.setText('0')
            self.lineFjRight.setText('0')
            self.lineqi.setText('0')
        self.Draw.update()

    def getValue(self):
        self.lineL.setText(str(Construction.Model.cornels[self.cornels.currentRow()].L))
        self.lineA.setText(str(Construction.Model.cornels[self.cornels.currentRow()].A))
        self.lineFjLeft.setText(str(Construction.Model.cornels[self.cornels.currentRow()].FjLeft))
        self.lineFjRight.setText(str(Construction.Model.cornels[self.cornels.currentRow()].FjRight))
        self.lineqi.setText(str(Construction.Model.cornels[self.cornels.currentRow()].qi))
        self.line_sMax.setText(str(Construction.Model.cornels[self.cornels.currentRow()].sMax))

    def setValue(self):
        if len(Construction.Model.cornels) != 0:
            # lineL
            if is_digit(self.lineL.text()) == 1:
                Construction.Model.cornels[self.cornels.currentRow()].L = int(self.lineL.text())

            if is_digit(self.lineL.text()) == 2:
                if Construction.Model.cornels[self.cornels.currentRow()].L > 0:
                    Construction.Model.cornels[self.cornels.currentRow()].L = float(self.lineL.text())
                if Construction.Model.cornels[self.cornels.currentRow()].L <= 0:
                    Construction.Model.cornels[self.cornels.currentRow()].L = int(self.lineL.text())*(-1)
                    self.lineL.setText(str(int(self.lineL.text())*(-1)))

            if is_digit(self.lineL.text()) == 3:
                self.lineL.setText('0')
                Construction.Model.cornels[self.cornels.currentRow()].L = 0

            # LineA
            if is_digit(self.lineA.text()) == 1:
                Construction.Model.cornels[self.cornels.currentRow()].A = int(self.lineA.text())

            if is_digit(self.lineA.text()) == 2:
                if Construction.Model.cornels[self.cornels.currentRow()].A > 0:
                    Construction.Model.cornels[self.cornels.currentRow()].A = float(self.lineA.text())
                if Construction.Model.cornels[self.cornels.currentRow()].A <= 0:
                    Construction.Model.cornels[self.cornels.currentRow()].A = int(self.lineA.text())*(-1)
                    self.lineA.setText(str(int(self.lineA.text())*(-1)))

            if is_digit(self.lineA.text()) == 3:
                self.lineA.setText('0')
                Construction.Model.cornels[self.cornels.currentRow()].A = 0

            # LineFjLeft
            if is_digit(self.lineFjLeft.text()) == 1:
                Construction.Model.cornels[self.cornels.currentRow()].FjLeft = int(self.lineFjLeft.text())
                if self.cornels.currentRow() != 0:
                    Construction.Model.cornels[self.cornels.currentRow()-1].FjRight = int(self.lineFjLeft.text())

            if is_digit(self.lineFjLeft.text()) == 2:
                if Construction.Model.cornels[self.cornels.currentRow()].FjLeft > 0:
                    Construction.Model.cornels[self.cornels.currentRow()].FjLeft = float(self.lineFjLeft.text())
                    if self.cornels.currentRow() != 0:
                        Construction.Model.cornels[self.cornels.currentRow() - 1].FjRight = float(self.lineFjLeft.text())
                if Construction.Model.cornels[self.cornels.currentRow()].FjLeft <= 0:
                    Construction.Model.cornels[self.cornels.currentRow()].FjLeft = float(self.lineFjLeft.text())
                    if self.cornels.currentRow() != 0:
                        Construction.Model.cornels[self.cornels.currentRow() - 1].FjRight = float(self.lineFjLeft.text())
                    self.lineFjLeft.setText(str(float(self.lineFjLeft.text())))

            if is_digit(self.lineFjLeft.text()) == 3:
                self.lineFjLeft.setText('0')
                Construction.Model.cornels[self.cornels.currentRow()].FjLeft = 0
                if self.cornels.currentRow() != 0:
                    Construction.Model.cornels[self.cornels.currentRow() - 1].FjRight = 0


            # LineFjRight
            if is_digit(self.lineFjRight.text()) == 1:
                Construction.Model.cornels[self.cornels.currentRow()].FjRight = int(self.lineFjRight.text())
                if self.cornels.currentRow() < len(Construction.Model.cornels)-1:
                    Construction.Model.cornels[self.cornels.currentRow()+1].FjLeft = int(self.lineFjRight.text())
                # if self.cornels.currentRow() != 0:
                #     Construction.Model.cornels[self.cornels.currentRow()-1].FjRight = int(self.lineFjLeft.text())

            if is_digit(self.lineFjRight.text()) == 2:
                if Construction.Model.cornels[self.cornels.currentRow()].FjRight > 0:
                    Construction.Model.cornels[self.cornels.currentRow()].FjRight = float(self.lineFjRight.text())
                    if self.cornels.currentRow() < len(Construction.Model.cornels) - 1:
                        Construction.Model.cornels[self.cornels.currentRow() + 1].FjLeft = float(self.lineFjRight.text())
                if Construction.Model.cornels[self.cornels.currentRow()].FjRight <= 0:
                    Construction.Model.cornels[self.cornels.currentRow()].FjRight = float(self.lineFjRight.text())
                    self.lineFjRight.setText(str(float(self.lineFjRight.text())))
                    if self.cornels.currentRow() != 0:
                        Construction.Model.cornels[self.cornels.currentRow() - 1].FjRight = float(self.lineFjLeft.text())
                    if self.cornels.currentRow() < len(Construction.Model.cornels) - 1:
                        Construction.Model.cornels[self.cornels.currentRow() + 1].FjLeft = float(self.lineFjRight.text())

            if is_digit(self.lineFjRight.text()) == 3:
                self.lineFjRight.setText('0')
                Construction.Model.cornels[self.cornels.currentRow()].FjRight = 0
                # if self.cornels.currentRow() < len(Construction.Model.cornels) - 1:
                #     Construction.Model.cornels[self.cornels.currentRow() + 1].FjLeft = 0

            # Lineqi
            if is_digit(self.lineqi.text()) == 1:
                Construction.Model.cornels[self.cornels.currentRow()].qi = int(self.lineqi.text())

            if is_digit(self.lineqi.text()) == 2:
                Construction.Model.cornels[self.cornels.currentRow()].qi = float(self.lineqi.text())

            if is_digit(self.lineqi.text()) == 3:
                self.lineqi.setText('0')

            # Line s_Max
            if is_digit(self.line_sMax.text()) == 1:
                Construction.Model.cornels[self.cornels.currentRow()].sMax = int(self.line_sMax.text())

            if is_digit(self.line_sMax.text()) == 2:
                Construction.Model.cornels[self.cornels.currentRow()].sMax = float(self.line_sMax.text())

            if is_digit(self.line_sMax.text()) == 3:
                self.line_sMax.setText('0')

        self.Draw.update()


    def moveSceneLeft(self):
        self.Draw.LeftScene()

    def moveSceneRight(self):
        self.Draw.RightScene()

    def sceneLess(self):
        self.Draw.SceneLess()

    def sceneMore(self):
        self.Draw.SceneMore()

    def createConstruction(self):
        if len(Construction.Model.cornels) != 0:
            # self.Draw.CreateConstruction()
            # Model.show()


            Construction.Model.fill()
            # Construction.Model.show()
            # Construction.Model.showMatrix()
            # print(Construction.Model.Answer)
            Construction.Model.calculateDelta()
            # print('Delta = ', Construction.Model.Delta)
            Construction.Model.calculateU()
            # print('U =', Construction.Model.U)
            # Construction.Model.show_U()

            # print('Продольные силы: ')
            Construction.Model.calculateN()
            # Construction.Model.show_N()
            #
            # print('Нормальные напряжения: ')
            Construction.Model.calculate_s()
            # Construction.Model.show_s()
            #
            # print('Перемещения: ')
            Construction.Model.calculate_u()
            # Construction.Model.show_u()

            Construction.Model.errors.clear()
            self.err = Error_s()
            for i in range(len(Construction.Model.cornels)):
                if fabs(Construction.Model.cornels[i].s_null) > Construction.Model.cornels[i].sMax or fabs(Construction.Model.cornels[i].s_L) > Construction.Model.cornels[i].sMax:
                    Construction.Model.errors.append(i)
            if len(Construction.Model.errors) != 0:
                self.err.f()
                self.err.show()

        self.Draw.update()


    def openProject(self):
        FileDialog = QFileDialog.getOpenFileName()
        try:
            self.Clear()
            f = open(FileDialog[0], 'rb')
            Buffer = pickle.load(f)
            Construction.Model.cornels = Buffer[0]
            Construction.Model.Seal = Buffer[1]
            f.close()
            self.Draw.update()
            self.updateProject()
        except IOError:
            print('No file selected')


    def saveProject(self):
        FileDialog = QFileDialog.getSaveFileName()
        try:
            f = open(FileDialog[0], 'wb')
            Buffer = [Construction.Model.cornels, Construction.Model.Seal]
            pickle.dump(Buffer, f)
            f.close()
        except IOError:
            print('No file selected')


    def updateProject(self):
        for i in range(len(Construction.Model.cornels)):
            self.cornels.addItem('Стержень')

        if Construction.Model.Seal == 'Заделка справа':
            self.KinematicConditions.setCurrentIndex(0)
        if Construction.Model.Seal == 'Заделка слева':
            self.KinematicConditions.setCurrentIndex(1)
        if Construction.Model.Seal == 'Заделка слева и справа':
            self.KinematicConditions.setCurrentIndex(2)

    def openTableResult(self):
        self.createConstruction()
        # print(Construction.Model.errors)
        if len(Construction.Model.errors) == 0:
            self.Table.fill()
            self.Table.show()

    def setTextKindLoad(self):
        self.textKindLoad = self.KindLoad.currentText()

    def resultComponent(self):
        if self.textKindLoad == 'Перемещения ux':
            if (float(self.lineValueRod.text()) >= 0) and (float(self.lineValueRod.text()) <= Construction.Model.cornels[self.cornels.currentRow()].L):
                Construction.Model.calculate_ux(self.cornels.currentRow(), float(self.lineValueRod.text()))
                ux = Construction.Model.cornels[self.cornels.currentRow()].u_x
                self.ValueComponent.setText('u(' + self.lineValueRod.text() + ') = ' + str(ux))
            else:
                self.ValueComponent.setText('Not a valid value x')

        if self.textKindLoad == 'Продольные силы Nx':
            if (float(self.lineValueRod.text()) >= 0) and (float(self.lineValueRod.text()) <= Construction.Model.cornels[self.cornels.currentRow()].L):
                Construction.Model.calculate_Nx(self.cornels.currentRow(), float(self.lineValueRod.text()))
                Nx = Construction.Model.cornels[self.cornels.currentRow()].N_x
                self.ValueComponent.setText('N(' + self.lineValueRod.text() + ') = ' + str(Nx))
            else:
                self.ValueComponent.setText('Not a valid value x')

        if self.textKindLoad == 'Нормальные напряжения σx':
            if (float(self.lineValueRod.text()) >= 0) and (float(self.lineValueRod.text()) <= Construction.Model.cornels[self.cornels.currentRow()].L):
                Construction.Model.calculate_sx(self.cornels.currentRow(), float(self.lineValueRod.text()))
                sx = Construction.Model.cornels[self.cornels.currentRow()].s_x
                self.ValueComponent.setText('σ(' + self.lineValueRod.text() + ') = ' + str(sx))
            else:
                self.ValueComponent.setText('Not a valid value x')


class ResultTable(QWidget):
    def __init__(self):
        super(ResultTable, self).__init__()
        QVB = QVBoxLayout(self)
        self.Table = QTableWidget()
        QVB.addWidget(self.Table)
        self.setWindowTitle("Results")

    def fill(self):
        self.Table.setColumnCount(5)
        self.Table.setRowCount(len(Construction.Model.cornels) * 2)
        self.Table.setHorizontalHeaderLabels(["Rod", "x", "N", "σ", "u"])
        for i in range(len(Construction.Model.cornels)):
            self.Table.setItem(i * 2, 0, QTableWidgetItem(str(i+1)))
            self.Table.setItem(i * 2 + 1, 0, QTableWidgetItem(str(i+1)))

            self.Table.setItem(i * 2, 1, QTableWidgetItem("0"))
            self.Table.setItem(i * 2 + 1, 1, QTableWidgetItem(str(Construction.Model.cornels[i].L)))

            self.Table.setItem(i * 2, 2, QTableWidgetItem(str(Construction.Model.cornels[i].N_null)))
            self.Table.setItem(i * 2 + 1, 2, QTableWidgetItem(str(Construction.Model.cornels[i].N_L)))

            self.Table.setItem(i * 2, 3, QTableWidgetItem(str(Construction.Model.cornels[i].s_null)))
            self.Table.setItem(i * 2 + 1, 3, QTableWidgetItem(str(Construction.Model.cornels[i].s_L)))

            self.Table.setItem(i * 2, 4, QTableWidgetItem(str(Construction.Model.cornels[i].u_null)))
            self.Table.setItem(i * 2 + 1, 4, QTableWidgetItem(str(Construction.Model.cornels[i].u_L)))

        self.setMinimumSize(560, 330)


class Error_s(QWidget):
    def __init__(self):
        super(Error_s, self).__init__()
        self.QVB = QVBoxLayout(self)
        self.Text = QLabel("σ максимальное превышает допустимое значение в стержнях")


    def f(self):
        StrError = ""
        for i in Construction.Model.errors:
            StrError = StrError + str(i) + ", "
        self.TextErr = QLabel(StrError)
        self.QVB.addWidget(self.Text)



if __name__ == '__main__':
    app = QApplication([])

    T = Main()
    T.show()

    app.setStyle(QStyleFactory.create('cleanlooks'))
    app.exec_()

